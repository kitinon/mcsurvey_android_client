﻿Type=Activity
Version=3.82
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False 
	#IncludeTitle: False
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
    Dim CheckDone = 0 As Int
	Dim CountZero = 0 As Int
	Dim StatusDaily =0 As Int
End Sub


Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.	
    Dim SQL1 As SQL 
	Dim Cursor2 As Cursor
	Private Button1 As Button
	Private Spinner1,Spinner2 As Spinner
	Private Button2 As Button 
	Private EditText3 As EditText
	Private Spinner3 As Spinner
	Private EditText7 As EditText
	Private Label2 As Label
	Private Label6 As Label
	Private Spinner5 As Spinner
	Private Spinner6 As Spinner
	Dim PageIndex, ItemCount As Int
	Private Label13 As Label
	Dim edit_date1,edit_date2 ,edit_date3 As xnEditDate 
	Private Label10 As Label
	Dim Type1,Type2,Type3 As String
	Private Button3 As Button
	Private CheckBox1 As CheckBox
	Private CheckBox2 As CheckBox
	Private CheckBox3 As CheckBox
	Private CheckBox4 As CheckBox
	Private EditText1 As EditText
	Private EditText2 As EditText
	Private EditText4 As EditText
	Private EditText6 As EditText
	Private Spinner4 As Spinner
	Dim Category As String
	Dim now As Long
    now = DateTime.now
	Private ImageView1 As ImageView
	Private Button4 As Button
	Dim type3_Posision =0 As Int
End Sub

Sub Activity_Create(FirstTime As Boolean)
	'Do not forget to load the layout file created with the visual designer. For example:
	Activity.LoadLayout("CheckDaily")	
    PageIndex = 0 
	ItemCount = 0
	Type1 = "ปกติ"
	Type2 = ""
	Spinner1.addall(Array As String("","ชำรุด","ใกล้หมดอายุ","หมดอายุ"))
	Spinner1.GetItem(0)
	Spinner2.addall(Array As String("","ยกเลิกขาย","สินค้าหลุด Plan","จัดโปรโมชั่นใหม่","Short Trade","ของค้างเครื่อง","ไม่มีจำหน่าย","Renovate Store","รอของเข้า"))
	Spinner2.GetItem(0)
	Spinner3.addall(Array As String("ปกติ","Mail","Instore","ห้างปรับราคา"))
	Spinner3.GetItem(0) 	
	Category = ""
   'ToastMessageShow(Tripplan.StoreID.SubString2(5,6),False)
    edit_date1.Initialize("edit1_date") 
   ' edit_date1.Format_sql = "dd-mm-yyyy"
	edit_date1.DateAsLong = now
	 
	Activity.AddView( edit_date1 , Spinner1.Left+40 , Label10.top  ,100dip, 50) 
	
	edit_date2.Initialize("edit2_date") 
    edit_date2.DateAsLong = now
	Activity.AddView( edit_date2 , edit_date1.Left+ 150 ,Label10.top,100dip, 50) 
	
	edit_date3.Initialize("edit3_date") 
	edit_date3.DateAsLong = now
	Activity.AddView( edit_date3 , EditText3.Left ,Spinner2.top,100dip, 50) 	
	edit_date3.Visible = False
	Get_Store
	Get_Cat
	Spinner5.Add("")
	If Spinner5.GetItem(0) <> "" Then
	 If DailyReport.ItemE <> "" Then
	  Get_ByItems
	 ' ImageView1.Visible = True
	  Button1.Visible = False
	  Button2.Visible = False
	  Else
	  ImageView1.Visible = False
	  'Button1.Visible = True
	   Button2.Visible = True
	End If 
	End If 
End Sub


Sub Get_ByItems
Dim MyDatabase As String
	MyDatabase = File.DirRootExternal &"/Hi-Q"
	Spinner5.Clear
	Spinner6.Clear	
	SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	Try     
  	 Cursor2 = SQL1.ExecQuery("select * from StoreInfo where Item = "&" '"& DailyReport.ItemE & "'" & " and BranchID  = '" & Tripplan.StoreID2.SubString(0) & "'" )
	 ItemCount = Cursor2.RowCount-1
	' For P = 0 To Cursor2.RowCount - 1
	 Cursor2.Position = 0
	  Spinner5.Add(Cursor2.GetString("Item"))
	  Spinner6.Add(Cursor2.GetString("Price"))
     ' Next
	  Cursor2.Close
	  Label6.Text = Spinner5.GetItem(0)
	  Label13.Text = Spinner6.GetItem(0)
	  PageIndex = 0
	  If ItemCount <> 0 Then
	   Button2.Enabled = True
	   Else
	   Button1.Enabled = False
	   Button2.Enabled = False
	  End If 	   
	Catch
		Log(LastException.Message)
	End Try 
End Sub	


Sub Get_UpdateItem
Dim MyDatabase As String
	MyDatabase = File.DirRootExternal &"/Hi-Q"
	Spinner5.Clear
	Spinner6.Clear	
	SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	Try     
  	 Cursor2 = SQL1.ExecQuery("select Distinct Item,Price from StoreInfo where BranchID  = '" & Tripplan.StoreID2.SubString(0) & "'  AND  Cat  = '" &   Category   & "' AND  status_check = 0" ) 
	 For P = 0 To Cursor2.RowCount - 1
	 Cursor2.Position = P
	 Spinner5.Add(Cursor2.GetString("Item"))
	 Spinner6.Add(Cursor2.GetString("Price"))
	Next
	 
	 If Cursor2.RowCount = 0 Then
	  Button2.Enabled = False
	  ToastMessageShow("จบ Categoly นี้แล้ว",False)
	  Label13.Text = "0"
	 End If
	  Cursor2.Close
	  Label6.Text = Spinner5.GetItem(0)
	  Label13.Text = Spinner6.GetItem(0)
	Catch
		Log(LastException.Message)
	End Try 
End Sub	
Sub Activity_Resume
  
End Sub

Sub Activity_Pause (UserClosed As Boolean)

End Sub

Sub insert_data
 Dim Col1,Col2,Col3,Col4,Col5,Col6,Col10,Col12,Col13,Col14,Col15,Col16,Col17,Col19,Col20,Col7,Col8,Col9,Col11,Col18,Col21 As String
' Dim Col7,Col8,Col9,Col11,Col18 As Int
  If type3_Posision = 8 Then
	  Col1 = edit_date3.DateAsSql
	 Else 
	  Col1 = ""
	 End If


 Col2 = 0
 Col3 = Label2.Text 'Store
 Col4 = EditText6.Text
 Col5 = Type3 
 Col6 = Type2
 Col7 = edit_date2.DateAsSql
 Col8 =edit_date1.DateAsSql
 
 If CheckBox4.Checked Then
  Col9 = 1
 Else
  Col9 = 0
 End If 
 If CheckBox3.Checked Then
  Col10 = 1
 Else
  Col10 = 0
 End If 
 If CheckBox2.Checked Then
  Col11 = 1
 Else
  Col11 = 0
 End If 
 Col12 = EditText2.Text
 Col13 =  Main.USerID  ' USerID
  
 Col14 = "23052014" 'Date
 Col15 =  Tripplan.StoreID2.SubString(0) 'StoreID
 Col16 = Label6.Text 
 Col17 =  Label13.Text
 Col18 =  EditText4.Text
 Col19 =  Type1
 If CheckBox1.Checked Then
  Col20 = 1
 Else
  Col20 = 0
 End If 
 
 Col21 = EditText1.Text
 SQL1.ExecNonQuery2("INSERT INTO DailyReport VALUES(?,?,NULL,?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?,?,?,?,?,?,?,?,?)", Array As Object (Col1,Col2,Col3,Col4,Col5,Col6,Col7,Col8,Col9,Col10,Col11,Col12,Col13,Col14,Col15,Col16,Col17,Col18,Col19,Col20,Col21)) 
 '("ปรบัราคา","ปกติ","20140521","20140522","11","12",0,1,0,บางนา","Hienz","พรก","300"))
 'SQL2.ExecNonQuery2("INSERT INTO table2 VALUES(?,?,?,?,?,?,?,?,?)", Array As Object("B1C2",etsocket.Text,etftp.Text,etport.Text,etftpport.Text,etusername.Text,etpassword.Text,etpath.Text,etdbname.Text))
 Update_Check
 DailyReport.ItemE = ""
 EditText3.Text = ""
 Spinner2.GetItem(0) 
 Type3 = ""
 Type2 = ""
 Type1 = ""
 EditText3.Visible = False
 edit_date3.Visible = False
End Sub 


Sub Update_Check 
Dim MyDatabase As String
   ' ToastMessageShow(Tripplan.StoreID2.SubString(0),False)
    MyDatabase = File.DirRootExternal &"/Hi-Q"
    SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	SQL1.ExecNonQuery("UPDATE StoreInfo SET status_check = 1 where Item = " & "'" & Label6.Text & "'" &" and  BranchID = "&"'"&   Tripplan.StoreID2.SubString(0) & "'")  ' where StoreID2  = '" & Tripplan.StoreID2.SubString(0) & "'")
	Cursor2.Close
	Get_UpdateItem
End Sub

Sub Get_Store  
Dim MyDatabase As String
    MyDatabase = File.DirRootExternal &"/Hi-Q"
    SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	Cursor2 = SQL1.ExecQuery("select Store,Branch from TripPlan where StoreID2  = '" & Tripplan.StoreID2.SubString(0) & "'")
	 If Cursor2.RowCount <> 0 Then
	  Cursor2.Position = 0
	 Label2.Text =  Cursor2.GetString("Store") &" "& Cursor2.GetString("Branch")
	 ' ToastMessageShow(Cursor2.GetString("Store") &" "& Cursor2.GetString("Branch") ,False)
     End If
    Cursor2.Close
End Sub

Sub Get_Cat
Dim MyDatabase As String
	MyDatabase = File.DirRootExternal &"/Hi-Q"
	'Msgbox(MyDatabase,MyDatabase) 
	SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	Try     
  	 Cursor2 = SQL1.ExecQuery("select distinct Cat from StoreInfo where BranchID  = '" & Tripplan.StoreID2.SubString(0) & "'")
	 'CountPage = Cursor2.RowCount
	 For P = 0 To Cursor2.RowCount - 1
	 Cursor2.Position = P
	 Spinner4.Add(Cursor2.GetString("Cat") )
      Next
	  Cursor2.Close
	Catch
		Log(LastException.Message)
	End Try 
	Category=Spinner4.GetItem(0)
	Get_Items
End Sub	


Sub Get_Items
Dim MyDatabase As String
	MyDatabase = File.DirRootExternal &"/Hi-Q"
	Spinner5.Clear
	Spinner6.Clear	
	'Msgbox(MyDatabase,MyDatabase) 
	SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	Try     
  	 Cursor2 = SQL1.ExecQuery("select * from StoreInfo where BranchID  = '" & Tripplan.StoreID2.SubString(0) & "'  AND  Cat  = '" &   Category   & "' and status_check = 0" )
	 ItemCount = Cursor2.RowCount
	 For P = 0 To Cursor2.RowCount - 1
	 Cursor2.Position = P
	  ' ToastMessageShow("Count= "&ItemCount,False)
	  Spinner5.Add(Cursor2.GetString("Item"))
	  Spinner6.Add(Cursor2.GetString("Price"))
	  
	  ' ListView1.AddTwoLines(Cursor2.GetString("StoreID"), Cursor2.GetString("Item") & " "  & Cursor2.GetString("Price") )
      Next
	  Cursor2.Close
	  Label6.Text = Spinner5.GetItem(0)
	  Label13.Text = Spinner6.GetItem(0)
	  PageIndex = 0
	  If ItemCount <> 0 Then
	   Button2.Enabled = True
	   Else
	   Button1.Enabled = False
	   Button2.Enabled = False
	  End If 	   
	Catch
		Log(LastException.Message)
	End Try 
End Sub	
 
Sub ListViewItems_ItemLongClick (Position As Int, Value As Object)
	
End Sub
Sub Button1_Click  'Back
    'insert_data
   ' Clear_Status
'	If PageIndex <> 0 Then	 
'	Button1.Enabled = True
'	Label6.Text = Spinner5.GetItem(PageIndex-1)
'	Label13.Text = Spinner6.GetItem(PageIndex-1)
'	Spinner5.SelectedIndex = PageIndex-1
'	PageIndex = PageIndex-1	
'	Else
'	 Button1.Enabled = False
'	End If
'	 'ToastMessageShow(PageIndex,False)
'	If PageIndex = 0 Then	 
'	'insert_data
'    'Clear_Status
'	Button1.Enabled = False
'	Button2.Enabled = True
'	End If
End Sub

 

Sub Button2_Click  'Next
    insert_data
    Clear_Status
  ' End If	
'	If PageIndex <= ItemCount Then	 
'	Button2.Enabled = True
'	Label6.Text = Spinner5.GetItem(PageIndex+1)
'	Label13.Text = Spinner6.GetItem(PageIndex+1)
'	Spinner5.SelectedIndex= PageIndex+1
'	PageIndex = PageIndex+1	
'	Else
'	 Button2.Enabled = False
'	End If	
'	'ToastMessageShow(PageIndex,False)
'	If PageIndex = ItemCount Then
'	 insert_data
'     Clear_Status
'     Button2.Enabled = False
'	 Button1.Enabled = True
'	End If
End Sub

Sub Spinner3_ItemClick (Position As Int, Value As Object)
  Type1 = Value
  If Position <> 3 Then
    EditText7.Text = ""
End If	
End Sub

Sub Spinner5_ItemClick (Position As Int, Value As Object)
	PageIndex = Position
	Label6.Text = Spinner5.GetItem(Position)
	Label13.Text = Spinner6.GetItem(Position)
End Sub

Sub Spinner2_ItemClick (Position As Int, Value As Object)
	Type3 = Value
	type3_Posision = Position
	If Position = 8 Then
	  edit_date3.Visible = True
	  EditText3.Text = edit_date3.DateAsSql 
	  EditText3.Visible = True
	 Else 
	  edit_date3.Visible = False
	  EditText3.Text = ""
	  EditText3.Visible = False
	 End If
	
End Sub

Sub Spinner1_ItemClick (Position As Int, Value As Object)
	Type2 = Value
End Sub

Sub Spinner4_ItemClick (Position As Int, Value As Object)
	Category = Value
	Get_Items
End Sub

Sub Clear_Status
  CheckBox1.Checked = False
  CheckBox2.Checked = False
  CheckBox3.Checked = False
  CheckBox4.Checked = False
  EditText1.Text = ""
  EditText2.Text = ""
  EditText4.Text = ""
  EditText6.Text = ""
  EditText7.Text = ""
  edit_date1.DateAsLong = now
  edit_date2.DateAsLong = now
  Spinner1.SelectedIndex = 0
  Spinner2.SelectedIndex = 0
  Spinner3.SelectedIndex = 0
End Sub 

Sub ImageView1_Click
	insert_data
	Clear_Status
End Sub

Sub Check_Completed
Dim MyDatabase As String
    MyDatabase = File.DirRootExternal &"/Hi-Q"
    SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	Cursor2 = SQL1.ExecQuery("select Count(*) as count from StoreInfo  where status_check ='0' and BranchID  = '" & Tripplan.StoreID2.SubString(0) & "'")
	'ToastMessageShow(Cursor2.RowCount,False)
	 If Cursor2.RowCount <> 0 Then    
	   Cursor2.Position = 0	    
	   CountZero =  Cursor2.GetString("count")
	   Cursor2.Close
     End If   
End Sub

Sub Activity_KeyPress (KeyCode As Int) As Boolean 'Return True to consume the event
		'back and check out
	If KeyCode = KeyCodes.KEYCODE_BACK   Then
	 If DailyReport.ItemE <> "" Then
        ToastMessageShow("บันทึกเรียบร้อย...",False)
 		insert_data
 		Clear_Status
      End If ' Edit 
	 Check_Completed
	  If CountZero > 0 Then 
	    StartActivity(DailyReport) 
		StatusDaily = 0
	   Else
        StatusDaily = 1   
	  End If 
	End If	
End Sub 