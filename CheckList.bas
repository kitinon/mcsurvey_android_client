﻿Type=Activity
Version=3.82
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False
	#IncludeTitle: False
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.

End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.
    Dim svList As ScrollView
	Dim lst As ClsCheckList
	Dim SQL1 As SQL 
	Dim Cursor2 As Cursor
	Dim ItemName As String
	Dim ListItem,CheckItem As List
    Dim Row,p As Int
	Private ListView1 As ListView
End Sub

Sub Activity_Create(FirstTime As Boolean)
	'Do not forget to load the layout file created with the visual designer. For example:
	Activity.LoadLayout("CheckList")
    Dim gd As GradientDrawable
	Dim C(2) As Int
	C(0) = Colors.Blue
	C(1) = Colors.Black
	gd.Initialize("TL_BR", C)
	Activity.Background = gd
    ItemName = ""
	' List generation
	Get_Item
	Dim Hght As Int
	Hght = 40dip
	lst.Initialize(Me, svList, "", "lst_Click", "", 1dip)
	lst.CheckedColor = Colors.RGB(255, 0, 0)
	lst.ExtensionColor = Colors.ARGB(128, 255, 255, 255)
	
	lst.AddHeader("สถานะการทำงาน - รหัสสินค้า")
		
	For p= 0 To Row -1
		lst.AddCustomItem(p, CreateItem(Hght, p), Hght)'
	Next
	 
	lst.ResizePanel
End Sub

Sub Get_Item 
Dim MyDatabase As String
    ListItem.Initialize
	CheckItem.Initialize
    MyDatabase = File.DirRootExternal &"/Hi-Q"
    SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	Cursor2 = SQL1.ExecQuery("select Distinct Item,status_check  from StoreInfo  where BranchID  = '" & Tripplan.StoreID2.SubString(0) & "'")
	'ToastMessageShow(Cursor2.RowCount,False)
	 If Cursor2.RowCount <> 0 Then
	 Row = Cursor2.RowCount
	  For j = 0 To Cursor2.RowCount -1  
	   Cursor2.Position = j
	   ItemName = Cursor2.GetString("Item") 
	   ListItem.Add(Cursor2.GetString("Item"))
	   CheckItem.Add(Cursor2.GetString("status_check"))
	  Next
	   Cursor2.Close
     End If   
End Sub

Sub CreateItem(h, i As Int) As Panel
		Dim pnl As Panel, lbl As Label, chk As CheckBox, edt As EditText
		pnl.Initialize("")
		chk.Initialize("ChkBx") 'View #0
		pnl.AddView(chk, 0, 0, 40dip, h)

		lbl.Initialize("") 'View #1
		'lbl.Text = "Product #" & i
		lbl.TextColor = Colors.White
		
		If CheckItem.Get(p) = "1" Then
		 chk.Checked = True
		End If
		
		lbl.Text = ListItem.Get(p)
		lbl.TextSize = 18
		lbl.Gravity = Gravity.CENTER_VERTICAL
		pnl.AddView(lbl, 45dip, 0, lst.getWidth - 130dip, h)
		
		edt.Initialize("") 'View #2
		edt.Visible = False
		edt.Hint = "Qty"
		edt.InputType = edt.INPUT_TYPE_NUMBERS
		pnl.AddView(edt, lst.getWidth - 81dip, 1dip, 80dip, h)
		Return pnl
End Sub
Sub Activity_Resume

End Sub

Sub Activity_Pause (UserClosed As Boolean)

End Sub
