﻿Type=Activity
Version=3.82
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False 
	#IncludeTitle: False
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
    Dim CheckDone = 0 As Int
	Dim CountZero = 0 As Int
	Dim Competitor =0 As Int
End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.
    Dim SQL1,SQL2 As SQL 
	Dim Cursor2 As Cursor
	Private Spinner3 As Spinner
	Private Spinner2 As Spinner
	Private Spinner1 As Spinner
	Private Spinner4 As Spinner
	Private Spinner5 As Spinner
	Dim GetBrand,GetDetail,Getsize,GetProductID As String
	Private EditText5 As EditText
	Dim edit_date1,edit_date2 As xnEditDate 
	Private Button1 As Button
	Private Label2 As Label
	Private CheckBox1 As CheckBox 
	Private EditText4 As EditText
	Dim Type1,Type2 As String
	Private Label4 As Label
	Dim now As Long
    now = DateTime.now
	Private ImageView1 As ImageView
	Private Button2 As Button
	Private Button3 As Button
	Private Label3 As Label
	Private Spinner6 As Spinner
End Sub

Sub Activity_Create(FirstTime As Boolean)
    
	Type1 = "ปกติ"
	Type2 = ""
	 
	'Do not forget to load the layout file created with the visual designer. For example:
	Activity.LoadLayout("CheckMatch")
    Spinner3.addall(Array As String("ปกติ","Mail","Instore","ห้างปรับราคา"))
	Spinner3.GetItem(0)
    Spinner2.addall(Array As String("","ยกเลิกขาย","สินค้าหลุด Plan","จัดโปรโมชั่นใหม่","สินค้าเปลี่ยนแพ็คเก็ต","ไม่มีจำหน่าย"))
	Spinner2.GetItem(0)
	GetBrand = ""
	GetDetail = ""
	Getsize  = ""
	Get_Store
	Get_Items
	edit_date1.Initialize("edit1_date") 
	edit_date1.DateAsLong = now
	Activity.AddView( edit_date1 , 20 , Label3.top,120dip, 50) 
	edit_date2.Initialize("edit2_date") 
    edit_date2.DateAsLong = now
	Activity.AddView( edit_date2 , 260 ,Label3.top,120dip, 50) 	
	Spinner5.Add("")
	 If CompetitorReport.ProductID <> "" Then
	  Button3.Visible = False
	  Button2.Visible = False
	  Get_EditItem
	  Else
	   Button3.Visible = True
	 End If  
	Competitor =0 
	Type1 = ""
	Type2 = ""
End Sub

Sub Get_Price
Dim MyDatabase As String    
	Label4.Text = ""
	MyDatabase = File.DirRootExternal &"/Hi-Q"
	SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	Try     
  	 Cursor2 = SQL1.ExecQuery("select Price from CompetitorInfo where ProductID  = '" &  GetProductID   & "' AND  StoreID  = '" & Tripplan.StoreID.SubString(0) & "'")
     	
	 For P = 0 To Cursor2.RowCount - 1
	 Cursor2.Position = P
	 Label4.Text = Cursor2.GetString("Price") 
      Next
	  Cursor2.Close
	  If Cursor2.RowCount > 0 Then
	   Button3.Enabled = True
	  Else 
	    Button3.Enabled = False
	  End If 
	  
	Catch
		Log(LastException.Message)
	End Try 
End Sub	

Sub Get_EditItem
Dim MyDatabase As String
Dim CountPage As Int
    Spinner5.Clear
	Spinner1.Clear
	Spinner4.Clear
	MyDatabase = File.DirRootExternal &"/Hi-Q"
	SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	Try     
	
	' Cursor2 = SQL1.ExecQuery("select distinct [ProductID],[Brand],[Detail],[Size],Price  from CompetitorInfo  where StoreID  = '" & Tripplan.StoreID.SubString(0) & "' AND  status_check = 0 AND ProductID  = '" &  CompetitorReport.ProductID   &"'"   ) 
	 Cursor2 = SQL1.ExecQuery("select distinct [ProductID],[Brand],[Detail],[Size],Price  from CompetitorInfo  where BranchID  = '" & Tripplan.StoreID2.SubString(0)& "' AND  status_check = 0 AND ProductID  = '" &  CompetitorReport.ProductID   &"'"   ) 
	
	 CountPage = Cursor2.RowCount
	 Cursor2.Position = 0
	 
	  Spinner1.Add(Cursor2.GetString("Brand")&" "&Cursor2.GetString("Detail")&" "&Cursor2.GetString("Size") )
	  Spinner4.Add(Cursor2.GetString("ProductID"))
	  Label4.Text = Cursor2.GetString("Price") 
	 
	  Cursor2.Close 
	Catch
		Log(LastException.Message)
	End Try 
	
	If CountPage > 0 Then
	 GetProductID = Spinner4.GetItem(0)
	 Get_Price
	 Get_Fields
	End If
End Sub	

Sub insert_data
 Dim Col1,Col2,Col3,Col4,Col5,Col6,Col10,Col11,Col12,Col13,Col7,Col8,Col9,Col14  As String
 
 Col1 = 0
 Col2 = Type2
 Col3 = Type1
 Col4 = edit_date1.DateAsSql
 Col5 = edit_date2.DateAsSql
 Col6 = EditText4.Text  'Promotion
 Col7 = Label4.Text  'Price
 
 If CheckBox1.Checked Then
  Col8 = 1
 Else
  Col8 = 0
 End If 
  
 Col9 =  Main.USerID 'UserID
 Col10 =  Tripplan.StoreID2.SubString(0) 'StoreID
  'Col10 =  Tripplan.StoreID.SubString(0) 'StoreID
 Col11 =  Label2.Text 'Store
 Col12 =  GetBrand
 Col13 =  GetDetail
 Col14 =  Getsize
 SQL1.ExecNonQuery2("INSERT INTO Competitor VALUES(?,NULL,?,?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?,?)", Array As Object (Col1,Col2,Col3,Col4,Col5,Col6,Col7,Col8,Col9,Col10,Col11,Col12,Col13,Col14)) 
 '("ปรบัราคา","ปกติ","20140521","20140522","11","12",0,1,0,บางนา","Hienz","พรก","300"))
 Update_Check
 CompetitorReport.ProductID = ""
 Type2 = ""
 Type1 = ""
End Sub 

Sub Clear_Status
  CheckBox1.Checked = False
  EditText4.Text = ""
  edit_date1.DateAsLong = now
  edit_date2.DateAsLong = now
  Spinner2.SelectedIndex = 0
  Spinner3.SelectedIndex = 0
End Sub 

Sub Get_Store  
Dim MyDatabase As String
    MyDatabase = File.DirRootExternal &"/Hi-Q"
    SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	Cursor2 = SQL1.ExecQuery("select Store from CompetitorInfo where StoreID  = '" & Tripplan.StoreID & "'")
	 If Cursor2.RowCount <> 0 Then
	 Cursor2.Position = 0
	 Label2.Text =  Cursor2.GetString("Store")  
	 ' ToastMessageShow(Cursor2.GetString("Store") &" "& Cursor2.GetString("Branch") ,False)
     End If
    Cursor2.Close
End Sub

Sub Get_Items
Dim MyDatabase As String
Dim CountPage As Int
    Spinner1.Clear
	Spinner4.Clear
	MyDatabase = File.DirRootExternal &"/Hi-Q"
	SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	Try     
	 Cursor2 = SQL1.ExecQuery("select distinct [ProductID],[Brand],[Detail],[Size]  from CompetitorInfo  where BranchID  = '" & Tripplan.StoreID2.SubString(0) & "' AND  status_check = 0" ) 
	 ' Cursor2 = SQL1.ExecQuery("select distinct [ProductID],[Brand],[Detail],[Size]  from CompetitorInfo  where StoreID  = '" & Tripplan.StoreID.SubString(0) & "' AND  status_check = 0" ) 
	 CountPage = Cursor2.RowCount
	 For P = 0 To Cursor2.RowCount - 1
	  Cursor2.Position = P
	  Spinner1.Add(Cursor2.GetString("Brand")&" "&Cursor2.GetString("Detail")&" "&Cursor2.GetString("Size") )
	  Spinner4.Add(Cursor2.GetString("ProductID"))
     Next
	 If Cursor2.RowCount = 0 Then
	  Button3.Enabled = False
	  ToastMessageShow("จบ Categoly นี้แล้ว",False)
	  Label4.Text = "0"
	  Else 
	  Button3.Enabled = True
	 End If
	  Cursor2.Close
	Catch
		Log(LastException.Message)
	End Try 
	
	
	If CountPage > 0 Then
	 GetProductID = Spinner4.GetItem(0)
	 Get_Price
	 Get_Fields
	End If
	 
End Sub	

Sub Update_Check 
Dim MyDatabase As String
    MyDatabase = File.DirRootExternal &"/Hi-Q"
    SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	'SQL1.ExecNonQuery("UPDATE CompetitorInfo SET status_check = 1 where StoreID = '"&  Tripplan.StoreID.SubString(0)  & "' and Brand  = '" &  GetBrand   & "'  and  Detail  = '" &  GetDetail   & "'  and  Size  = '" &  Getsize   & "'")  
	SQL1.ExecNonQuery("UPDATE CompetitorInfo SET status_check = 1 where BranchID  = '" & Tripplan.StoreID2.SubString(0)   & "' and Brand  = '" &  GetBrand   & "'  and  Detail  = '" &  GetDetail   & "'  and  Size  = '" &  Getsize   & "'")  
	
	Get_Items
End Sub


Sub Spinner3_ItemClick (Position As Int, Value As Object)
	Type1 = Value
End Sub

Sub Spinner2_ItemClick (Position As Int, Value As Object)
	Type2 = Value
End Sub

Sub Get_Fields
   Dim MyDatabase As String
	SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	Try     
  	' Cursor2 = SQL1.ExecQuery("select  [Brand],[Detail],[Size] from CompetitorInfo where StoreID  = '" & Tripplan.StoreID.SubString(0) & "' AND productID = '"& GetProductID &"'")
	Cursor2 = SQL1.ExecQuery("select  [Brand],[Detail],[Size] from CompetitorInfo where BranchID  = '" & Tripplan.StoreID2.SubString(0) & "' AND productID = '"& GetProductID &"'")

 
	 Cursor2.Position = 0
	 If Cursor2.RowCount > 0 Then
	  GetBrand = Cursor2.GetString("Brand")  
	  GetDetail = Cursor2.GetString("Detail")  
	  Getsize = Cursor2.GetString("Size")   
	 End If
	 Cursor2.Close
	Catch
		Log(LastException.Message)
	End Try  
End Sub

Sub Spinner1_ItemClick (Position As Int, Value As Object)    
	GetProductID = Spinner4.GetItem(Position)
	Get_Price
	Get_Fields
End Sub
 
Sub Button1_Click
 Get_Fields
 insert_data
 Clear_Status
End Sub

Sub ImageView1_Click
 insert_data
 Clear_Status
End Sub

Sub Button3_Click
 insert_data
 Clear_Status	
End Sub

Sub Button2_Click
 		Msgbox("select distinct Brand,Detail,Size,Price  from CompetitorInfo where Brand  = '" &  GetBrand   & "'  and  Detail  = '" &  GetDetail   & "'  and  Size  = '" &  Getsize   & "' and  StoreID  = '" & Tripplan.StoreID.SubString(0) & "' AND  status_check = 0","")

End Sub

Sub Check_Completed
Dim MyDatabase As String
    MyDatabase = File.DirRootExternal &"/Hi-Q"
    SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	'Cursor2 = SQL1.ExecQuery("select Count(*) as count from CompetitorInfo  where status_check ='0' and StoreID  = '" & Tripplan.StoreID.SubString(0) & "'")
	Cursor2 = SQL1.ExecQuery("select Count(*) as count from CompetitorInfo  where status_check ='0' and BranchID  = '" & Tripplan.StoreID2.SubString(0) & "'")
	
	 If Cursor2.RowCount <> 0 Then    
	   Cursor2.Position = 0	    
	   CountZero =  Cursor2.GetString("count")
	   Cursor2.Close
     End If   
End Sub

Sub Activity_KeyPress (KeyCode As Int) As Boolean 'Return True to consume the event
	If KeyCode = KeyCodes.KEYCODE_BACK   Then
	 If CompetitorReport.ProductID <> "" Then
	  ToastMessageShow("บันทึกเรียบร้อย...",False)
	    Get_Fields
	    insert_data
	    Clear_Status	   
	  End If
	  Check_Completed
	   If CountZero > 0 Then 
	   StartActivity(CompetitorReport) 
	   Menu.Can_Delete_TripPlan = 0
	   Competitor =0
	   Else
	   Menu.Can_Delete_TripPlan = 1
	   Competitor =1
	  End If  
	End If	
End Sub