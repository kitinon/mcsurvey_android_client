﻿Type=Activity
Version=3.82
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False 
	#IncludeTitle: False
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
     Dim URLAdd,CheckAllProduct  As String
	 Dim Check1,Check2,Check3,Check4 As Int
	 Dim CheckAll=0 As Int
End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.
	Private EditText3 As EditText
	Private ImageView1 As ImageView
	Private ImageView2 As ImageView
	Private CheckBox1 As CheckBox
	Private CheckBox2 As CheckBox
	Private CheckBox3 As CheckBox
	Private CheckBox4 As CheckBox
	Dim SQL1 As SQL
	Private CheckBox5 As CheckBox
	Private Button1 As Button
End Sub

Sub Activity_Create(FirstTime As Boolean)
	'Do not forget to load the layout file created with the visual designer. For example:
	Activity.LoadLayout("CheckProduct")     
	URLAdd = EditText3.Text 
	CheckBox1.Checked = True
	CheckBox2.Checked = True
	CheckBox3.Checked = True
	CheckBox4.Checked = True
	
	CheckAllProduct =  File.ReadString(File.DirRootExternal & "/"& "Hi-Q", "CheckAll.txt")
	'ToastMessageShow(CheckAllProduct,False)                                 
	
	If CheckAllProduct = "1" Then
	  CheckBox5.Checked = True
	 Else
	  CheckBox5.Checked = False
	 End If
End Sub

Sub Activity_Resume
    CheckAllProduct =  File.ReadString(File.DirRootExternal & "/"& "Hi-Q", "CheckAll.txt") 
	'ToastMessageShow(CheckAllProduct,False)
	
	If CheckAllProduct = "1" Then
	  CheckBox5.Checked = True
	 Else
	  CheckBox5.Checked = False
	 End If
End Sub

Sub Activity_Pause (UserClosed As Boolean)

End Sub

Sub ListView1_ItemLongClick (Position As Int, Value As Object)
End Sub
Sub Button2_Click
	
End Sub
Sub Button1_Click
End Sub
Sub ListView1_ItemClick (Position As Int, Value As Object)
	
End Sub
Sub Spinner1_ItemClick (Position As Int, Value As Object)
End Sub
Sub Spinner3_ItemClick (Position As Int, Value As Object)
	
End Sub
Sub ImageView1_Click
File.WriteString(File.DirRootExternal & "/Hi-Q", "Initialize.txt", EditText3.Text)

Dim result As Int
' Dim pass As EditText
' pass.Initialize("password")
' pass.Text = "password"
' Activity.AddView(pass,100,530,140,140)
result = Msgbox2("กรุณายืนยัน", "ต้องการล้างข้อมูลทั้งหมด", "ตกลง", "", "ยกเลิก", LoadBitmap(File.DirAssets, "blue_recycle_bin.png"))
If result = DialogResponse.Positive Then
' ToastMessageShow(pass.Text,False)
 Clear_Table
  
End If

End Sub
Sub ImageView2_Click
Dim result As Int
result = Msgbox2("กรุณายืนยัน","ต้องการสำรองข้อมูลทั้งหมด", "ตกลง", "", "ยกเลิก", LoadBitmap(File.DirAssets, "keyboard.png"))
If result = DialogResponse.Positive Then
 ToastMessageShow("เรียบร้อยแล้ว",False)
End If
 
End Sub

Sub SetShowWhenLocked
   Dim r As Reflector
   r.Target = r.GetActivity
   r.Target = r.RunMethod("getWindow")
   r.RunMethod2("addFlags", 1111, "java.lang.int")
End Sub

Sub Clear_Table
 Dim MyDatabase As String
 MyDatabase = File.DirRootExternal &"/Hi-Q"
 SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	 
 SQL1.ExecNonQuery("DELETE FROM CheckIn ") 
 ToastMessageShow("Delete table CheckIn completed..",False)
 
 SQL1.ExecNonQuery("DELETE FROM DailyReport ") 
 ToastMessageShow("Delete table DailyReport completed..",False)
 
 SQL1.ExecNonQuery("DELETE FROM Competitor ") 
 ToastMessageShow("Delete table Competitor completed..",False)
 
 SQL1.ExecNonQuery("DELETE FROM TripPlan ") 
 ToastMessageShow("Delete table TripPlan completed..",False)
 ToastMessageShow("เรียบร้อยแล้ว",False)
 
End Sub


Sub CheckBox4_CheckedChange(Checked As Boolean)
  If Checked Then
    Check4 = 1
  Else
    Check4 = 0
   End If	
End Sub
Sub CheckBox3_CheckedChange(Checked As Boolean)
	 If Checked Then
    Check3 = 1
  Else
    Check3 = 0
   End If
End Sub
Sub CheckBox2_CheckedChange(Checked As Boolean)
  If Checked Then
    Check4 = 1
  Else
    Check4 = 0
   End If	
End Sub
Sub CheckBox1_CheckedChange(Checked As Boolean)
	 If Checked Then
    Check1 = 1
  Else
    Check1 = 0
   End If
End Sub
Sub CheckBox5_CheckedChange(Checked As Boolean)
  If Checked Then
	File.WriteString(File.DirRootExternal & "/"& "Hi-Q", "CheckAll.txt","1")
	CheckAllProduct = "1"
   Else
    File.WriteString(File.DirRootExternal & "/"& "Hi-Q", "CheckAll.txt","0")
	CheckAllProduct = "0"
  End If 	
End Sub