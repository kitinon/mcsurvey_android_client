﻿Type=Activity
Version=3.82
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False
	#IncludeTitle: False
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
    Dim ProductID = "" As String
	Dim CountZero = 0 As Int
End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.
    Dim SQL1 As SQL 
	Dim Cursor2 As Cursor
	Private ImageView1 As ImageView
	Private Label1 As Label
	Private ListView1 As ListView
End Sub

Sub Activity_Create(FirstTime As Boolean)
	'Do not forget to load the layout file created with the visual designer. For example:
	Activity.LoadLayout("CompetitorReport")
	ListView1.SingleLineLayout.Label.TextSize = 18dip
    ListView1.FastScrollEnabled = True
    ListView1.SingleLineLayout.ItemHeight = 45dip
    ListView1.SingleLineLayout.Label.TextColor = Colors.DarkGray
    Get_Item
	Menu.Can_Delete_TripPlan = 0
End Sub

Sub Activity_Resume
   Get_Item
   Menu.Can_Delete_TripPlan = 0
End Sub

Sub Activity_Pause (UserClosed As Boolean)

End Sub


Sub Get_Item 
'ToastMessageShow(Tripplan.StoreID.SubString(1),False)
Dim MyDatabase As String
 ListView1.Clear
    MyDatabase = File.DirRootExternal &"/Hi-Q"
    SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	'Cursor2 = SQL1.ExecQuery("select Distinct ProductID, Brand,Detail,Size  from CompetitorInfo  where status_check ='0' and StoreID  = '" & Tripplan.StoreID.SubString(0)  & "'")
	Cursor2 = SQL1.ExecQuery("select Distinct ProductID, Brand,Detail,Size  from CompetitorInfo  where status_check ='0' and BranchID  = '" & Tripplan.StoreID2.SubString(0) & "'")
	
	'ToastMessageShow(Cursor2.RowCount,False)
	
	 If Cursor2.RowCount <> 0 Then
	  For j = 0 To Cursor2.RowCount -1  
	   Cursor2.Position = j	    
	   ListView1.AddTwoLinesAndBitmap( Cursor2.GetString("ProductID"), Cursor2.GetString("Brand")&"/"&Cursor2.GetString("Detail")&"/"&Cursor2.GetString("Size"), LoadBitmap(File.DirAssets, "mud.png"))
	  Next
	   Cursor2.Close
	    Else
	   Activity.Finish
     End If   
End Sub

Sub ListView1_ItemClick (Position As Int, Value As Object)
Dim result As Int
result = Msgbox2("กรุณายืนยัน",  "ต้องการสำรวจรายการนี้ "   , "ตกลง", "", "ยกเลิก", LoadBitmap(File.DirAssets, "can.png"))
If result = DialogResponse.Positive Then
 	ProductID  = Value
	'ToastMessageShow(ProductID,False)
	StartActivity(CheckMatch)
End If
End Sub

Sub Check_Completed
'ToastMessageShow(Tripplan.StoreID.SubString(1),False)
Dim MyDatabase As String
    MyDatabase = File.DirRootExternal &"/Hi-Q"
    SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	Cursor2 = SQL1.ExecQuery("select Count(*) as count from CompetitorInfo  where status_check ='0' and    BranchID  = '" & Tripplan.StoreID2.SubString(0) & "'")
	'Cursor2 = SQL1.ExecQuery("select Count(*) as count from CompetitorInfo  where status_check ='0' and StoreID  = '" & Tripplan.StoreID.SubString(0) & "'")
	
	'ToastMessageShow(Cursor2.RowCount,False)
	 If Cursor2.RowCount <> 0 Then    
	   Cursor2.Position = 0	    
	   CountZero =  Cursor2.GetString("count")
	   Cursor2.Close
     End If   
End Sub

Sub Activity_KeyPress (KeyCode As Int) As Boolean 'Return True to consume the event
	If KeyCode = KeyCodes.KEYCODE_BACK   Then 
	     Check_Completed		  
	  If CountZero > 0 Then 
	        Msgbox(" ท่านยังสำรวจข้อมูลสินค้าไม่สมบูรณ์","ข้อความเตือน" )
	      If CheckProduct.CheckAllProduct = "1" Then
		   Return True  
		  End If 
		  Menu.Can_Delete_TripPlan = 0
		 Else
          Menu.Can_Delete_TripPlan = 1
		  Return False 
	  End If  
	End If	
End Sub
 