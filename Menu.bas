﻿Type=Activity
Version=3.82
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
    #FullScreen: False 
	#IncludeTitle: False
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
    Dim Book_Time As String
	Dim CheckCoutCompleted = 0 As Int
	Dim Can_Delete_TripPlan =0 As Int
End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.
    Dim SQL1,SQL2 As SQL 
	Dim Cursor2,Cursor1 As Cursor
	Private Button1 As Button
	Private Button2 As Button
	Private Button3 As Button
	Private Label1 As Label
	Private Label2 As Label
	Dim TripDate = "" As String
	Dim Post_CheckIn,Post_Com,Post_Daily,PostUpdateSync As HttpJob
	Private ImageView5 As ImageView
	Private Panel2 As Panel
	Private Button4 As Button
	Private Button5 As Button
	Dim Today As String
End Sub

Sub Activity_Create(FirstTime As Boolean)
	'Do not forget to load the layout file created with the visual designer. For example:
	Activity.LoadLayout("Menu")
    Label1.Text = Main.GPS_Latitude & "  |  "& Main.GPS_Longitude  
	Label2.Text = Main.GPS_info
	Book_Time = ""
	Can_Delete_TripPlan = 0
	Check_Completed
	If CheckMatch.Competitor = 1 Then
     Button3.Enabled = False
	 Button1.Enabled = False
	Else
	 Button3.Enabled = True
	 Button1.Enabled = False
	End If 
	If CheckDaily.StatusDaily = 1 Then
	 Button3.Enabled = True
	 Button1.Enabled = False
	Else
	 Button3.Enabled = False
	 Button1.Enabled = True
	End If 
	If  Can_Delete_TripPlan = 1 Then
	 Button3.Enabled = False
	 Button1.Enabled = False
	 Else
	 'Button3.Enabled = True
	 Button1.Enabled = True
	End If 
   
End Sub

Sub Activity_Resume
    Book_Time = ""
	Check_Completed
	If CheckMatch.Competitor = 1 Then
     Button3.Enabled = False
	 Button1.Enabled = False
	Else
	 Button3.Enabled = True
	 Button1.Enabled = False
	End If 
	If CheckDaily.StatusDaily = 1 Then
	 Button3.Enabled = True
	 Button1.Enabled = False
	 Else
	 Button3.Enabled = False
	 Button1.Enabled = True
	End If 
	If  Can_Delete_TripPlan = 1 Then
	 Button3.Enabled = False
	 Button1.Enabled = False
	 Else
	' Button3.Enabled = True
	 Button1.Enabled = True
	End If 
 
End Sub

Sub Button3_Click
	StartActivity(CheckMatch)
End Sub
Sub Button2_Click
	'StartActivity(CheckProduct)
End Sub
Sub Button1_Click
   ' ToastMessageShow(DateUtils.TicksToString(DateTime.Now),False)
	Book_Time = DateUtils.TicksToString(DateTime.Now)
	
	StartActivity(CheckDaily)
End Sub

Sub Check_Out
 Dim MyDatabase As String
 MyDatabase = File.DirRootExternal &"/Hi-Q"
 SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	
 Dim Col1,Col4,Col2 As String
 'ToastMessageShow("Check Out...",False)
 Col1 =  Tripplan.DateIn
 Col2 =  Tripplan.StoreID2.SubString(0)
 Col4 = DateUtils.TicksToString(DateTime.Now)
 SQL1.ExecNonQuery2("UPDATE CheckIn SET CheckOut = ?  WHERE Datetime = '" & Col1 & "'  AND  StoreID  = '" &   Col2   & "'" ,Array  As Object (Col4)) 
 'SQL1.ExecNonQuery2("UPDATE CheckIn SET CheckOut = ?  " ,Array As String("05/20/2014 13.55")) 
 
End Sub

Sub Clear_Flag_Check
Dim MyDatabase As String
 	MyDatabase = File.DirRootExternal &"/Hi-Q"
    SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	
	SQL1.ExecNonQuery("UPDATE StoreInfo SET status_check = 0 ")  ' where StoreID2  = '" & Tripplan.StoreID2.SubString(0) & "'")
	
    SQL1.ExecNonQuery("UPDATE CompetitorInfo SET status_check = 0 ")  ' where StoreID2  = '" & Tripplan.StoreID2.SubString(0) & "'")
    	
 
End Sub

Sub JobDone(Job As HttpJob) 
   Log("JobName = " & Job.JobName & ", Success = " & Job.Success)
   If Job.Success = True Then
      Select Job.JobName         
         Case "CheckIn"
          ToastMessageShow("Check in done",False) 
		 Case "UpdateStatus_Sync"
          ToastMessageShow("update sync done",False)
		  update_sync_CheckIn
		 Case "DailyReport"
          ToastMessageShow("DailyReport done",False) 
		  update_sync_Dailyreport
		 Case "Competitor"
          ToastMessageShow("Competitor done",False) 
		  Update_sync_Competitor
      End Select
	  'Activity.Finish 
	  'StartActivity(Tripplan)
   Else
      Log("Error: " & Job.ErrorMessage)
      ToastMessageShow("Error: " & Job.ErrorMessage, True)
   End If
   Job.Release
	
End Sub

Sub update_sync_Dailyreport 
    SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
		'Clear status flag sync
	SQL1.ExecNonQuery("UPDATE DailyReport SET status_sync = 1 ")  ' where StoreID2  = '" & Tripplan.StoreID2.SubString(0) & "'")
	ToastMessageShow("Clear flag DailyReport Sync Done",False)
End Sub

Sub update_sync_CheckIn
    SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
		'Clear status flag sync
	SQL1.ExecNonQuery("update CheckIn set sync = 1 ")  ' where StoreID2  = '" & Tripplan.StoreID2.SubString(0) & "'")
	ToastMessageShow("Clear flag Sync CheckIn Done",False)
	Delete_TripPlan
End Sub

Sub Delete_TripPlan
    SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
		'Clear status flag sync
		TripDate = DateTime.GetDayOfMonth(DateTime.Now)&"/"&DateTime.GetMonth(DateTime.Now)&"/"&DateTime.GetYear(DateTime.Now)
	SQL1.ExecNonQuery("Update TripPlan set status_sync = 1 " & " where UserID = '" &  Main.UserID & "' AND Date_Trip  = '" &  TripDate  & "' AND STOREID2=  '" &  Tripplan.StoreID2  &"'")
   	
	'ToastMessageShow("Update TripPlan Done",False)
	'Msgbox("Update TripPlan set status_sync = 1 " & " where UserID = '" &  Main.UserID & "' AND Date_Trip  = '" &  TripDate  & "' AND STOREID2=  '" &  Tripplan.StoreID2  &"'","")
End Sub

Sub Update_sync_Competitor
    SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	SQL1.ExecNonQuery("UPDATE Competitor SET status_sync = 1 ")  ' where StoreID2  = '" & Tripplan.StoreID2.SubString(0) & "'")
	ToastMessageShow("Clear flag Sync Competitor Done",False)
	'Panel2.Visible = False 
End Sub

Sub Activity_KeyPress (KeyCode As Int) As Boolean 'Return True to consume the event 
Dim result As Int
	'back and check out
	If KeyCode = KeyCodes.KEYCODE_BACK   Then
	 result = Msgbox2("กรุณายืนยัน","ต้องการ Check Out", "ตกลง", "", "ยกเลิก", LoadBitmap(File.DirAssets, "locationsurvey.png"))
	' End If	
	 If result = DialogResponse.Positive Then
	  ' If Panel2.Visible = False Then
	    Check_Out
	    'Clear_Flag_Check
		If Can_Delete_TripPlan =1 Then
		 Delete_TripPlan
		End If
		
		ToastMessageShow("Check Out...",False)
        StartActivity(Tripplan)
	 Else
		 Return True
		End If

	End If	
	 
End Sub

Sub PostUserTimeStamp  'Auto Sync
ToastMessageShow("Auto Sync User Time Stamp ...",False)
Dim CountforJson = 0 As Int
    TripDate = ""
   	SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
  Try     
  	Cursor1 = SQL1.ExecQuery("select UserID,sync,Datetime,CheckIn,CheckOut,StoreID,Lat,Long,location from CheckIn where UserID = '" & Main.UserID & "'" & " and sync =0 ")
	CountforJson = Cursor1.RowCount
	Dim InsertData1 As List
	InsertData1.Initialize
	Dim PostData1 As Map
	PostData1.Initialize
	
	Dim InsertData2 As List
	InsertData2.Initialize
	Dim PostData2 As Map
	PostData2.Initialize 
    'ToastMessageShow(Cursor1.RowCount,False)
	Post_CheckIn.Initialize ("CheckIn", Me)	
	For P = 0 To Cursor1.RowCount-1
	Cursor1.Position = P 
    Today = DateTime.GetMonth(DateTime.Now)&"/"&DateTime.GetDayOfMonth(DateTime.Now)&"/"&DateTime.GetYear(DateTime.Now) & " " & DateTime.GetHour(DateTime.Now)&":"&DateTime.GetMinute(DateTime.Now)
	 
	Dim objMap1 As Map: objMap1.Initialize
	objMap1.Clear
	objMap1.Put("UserID",Cursor1.GetString("UserID"))
	objMap1.Put("Date",Cursor1.GetString("Datetime"))
	objMap1.Put("CheckIn", Cursor1.GetString("CheckIn"))
	objMap1.Put("CheckOut", Cursor1.GetString("CheckOut"))
	objMap1.Put("StoreID", Cursor1.GetString("StoreID"))
	objMap1.Put("Latitude", Cursor1.GetString("Lat"))
	objMap1.Put("Longitude", Cursor1.GetString("Long"))
	objMap1.Put("Location", Cursor1.GetString("Location"))
	objMap1.Put("DateSync", Today)
    InsertData1.Add(objMap1)
	
	Dim objMap2 As Map: objMap2.Initialize
	TripDate = DateTime.GetDayOfMonth(DateTime.Now)&"/"&DateTime.GetMonth(DateTime.Now)&"/"&DateTime.GetYear(DateTime.Now)
	objMap2.Clear
	objMap2.Put("UserID",Cursor1.GetString("UserID"))
	'objMap2.Put("Trip_Date","'"&DateTime.GetDayOfMonth(Cursor1.GetString("Datetime"))&"/"&DateTime.GetMonth(Cursor1.GetString("Datetime"))&"/"&DateTime.GetYear(Cursor1.GetString("Datetime"))&"'" )
    objMap2.Put("Trip_Date",TripDate)
	'objMap2.Put("Trip_Date1","17/69/2014")
	objMap2.Put("BranchID", Cursor1.GetString("StoreID"))
    InsertData2.Add(objMap2)
    'ToastMessageShow(TripDate,False) 
   Next    
    Cursor1.Close	 
	If CountforJson > 0 Then
		PostData1.Put("insert", InsertData1)
		Dim objJSon1 As JSONGenerator: objJSon1.Initialize(PostData1) 
	    'ToastMessageShow(Intsert_CheckIn,False) 
		Post_CheckIn.PostString(Main.Intsert_CheckIn, objJSon1.ToPrettyString(2))
		'Msgbox(InsertData1,"Gen JSON") 
	    'File.WriteString(File.DirRootExternal & "/Hi-Q", "UserTimeStamp.txt",  objJSon1.ToPrettyString(2))
	 	
	    PostUpdateSync.Initialize("UpdateStatus_Sync", Me)	 
	    PostData2.Put("Update", InsertData2)
	    Dim objJSon2 As JSONGenerator: objJSon2.Initialize(PostData2) 
	  
		'Msgbox(objJSon2.ToPrettyString(4),"Gen JSON") 
	 	PostUpdateSync.PostString(Main.UpdateSync, objJSon2.ToPrettyString(2))
		'File.WriteString(File.DirRootExternal & "/Hi-Q", "TripplanSync.txt",  objJSon2.ToString)
	End If	
	Catch
		Log(LastException.Message)
	End Try 
	
	
End Sub

Sub PostCompetitor
Dim CountforJson = 0 As Int
    ToastMessageShow("Sync Competitor รอสักครู่....",False)
	SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
  Try     
  	Cursor1 = SQL1.ExecQuery("select Comment,Type,End,Start,Promotion,Price,Check1,UserID,StoreID,Store,Brand,Detail,Size from Competitor where UserID = '"  & Main.UserID &"'" & " and status_sync = 0")
	CountforJson = Cursor1.RowCount   
	Dim InsertData As List
	InsertData.Initialize
	Dim PostData As Map
	PostData.Initialize 
	ToastMessageShow("Competitor->"&Cursor1.RowCount,False)
	For P = 0 To Cursor1.RowCount-1
	Cursor1.Position = P 
    Today = DateTime.GetMonth(DateTime.Now)&"/"&DateTime.GetDayOfMonth(DateTime.Now)&"/"&DateTime.GetYear(DateTime.Now) & " " & DateTime.GetHour(DateTime.Now)&":"&DateTime.GetMinute(DateTime.Now)
	
	Post_Com.Initialize ("Competitor", Me)	 
	Dim objMap As Map: objMap.Initialize
	objMap.Clear
	objMap.Put("UserID",Cursor1.GetInt("UserID"))
	objMap.Put("StoreID",Cursor1.GetString("StoreID"))
	objMap.Put("Brand", Cursor1.GetString("Brand"))
	objMap.Put("Detail", Cursor1.GetString("Detail"))
	objMap.Put("Size", Cursor1.GetString("Size"))
	objMap.Put("Check1", Cursor1.GetString("Check1"))
	objMap.Put("Price", Cursor1.GetDouble("Price"))
	objMap.Put("Promotion", Cursor1.GetDouble("Promotion"))
	objMap.Put("StartDate", Cursor1.GetString("Start"))
	objMap.Put("EndDate", Cursor1.GetString("End"))
	objMap.Put("PromotionType", Cursor1.GetString("Type"))
	objMap.Put("Comment", Cursor1.GetString("Comment"))
	objMap.Put("DateSync", Today)
    InsertData.Add(objMap) 
 
   Next 
    Cursor1.Close	
	If CountforJson > 0 Then
		PostData.Put("insert", InsertData)
		Dim objJSon As JSONGenerator: objJSon.Initialize(PostData) 
	    Post_Com.PostString(Main.Intsert_Competitor, objJSon.ToPrettyString(2))
		'Msgbox(objJSon.ToPrettyString(4),"Gen JSON") 
		
		'File.WriteString(File.DirRootExternal & "/Hi-Q", "Competitor.txt",  objJSon.ToPrettyString(4))
	End If
   Catch
		Log(LastException.Message)
	End Try 
	
	 	
End Sub


Sub PostDailyReport
Dim CountforJson = 0 As Int
    ToastMessageShow("Sync Daily Report รอสักครู่....",False)
	
	SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
  Try    
   
  	Cursor1 = SQL1.ExecQuery("select Shipdate,Store,DMS,Comment,case_error,End,Unit,Start,Check4,Check3,Check2,Check1,Carton,UserID,Date,StoreID,Item,Price,Promotion,Type from DailyReport where UserID = '" & Main.UserID &"'" & "  and status_sync = 0")
	CountforJson = Cursor1.RowCount  
	Dim InsertData As List
	InsertData.Initialize
	Dim PostData As Map
	PostData.Initialize 
	ToastMessageShow("DailyReport->"&Cursor1.RowCount,False)
	For P = 0 To Cursor1.RowCount-1
	Cursor1.Position = P 
    Today = DateTime.GetMonth(DateTime.Now)&"/"&DateTime.GetDayOfMonth(DateTime.Now)&"/"&DateTime.GetYear(DateTime.Now) & " " & DateTime.GetHour(DateTime.Now)&":"&DateTime.GetMinute(DateTime.Now)
	Post_Daily.Initialize ("DailyReport", Me)	 
	
	Dim objMap As Map: objMap.Initialize
	
	objMap.Clear
	objMap.Put("UserID",Cursor1.GetInt("UserID"))
	objMap.Put("StoreID",Cursor1.GetString("StoreID"))
	objMap.Put("Item", Cursor1.GetString("Item"))
	objMap.Put("Check1", Cursor1.GetString("Check1"))
	objMap.Put("Check2", Cursor1.GetString("Check2"))
	objMap.Put("Check3", Cursor1.GetString("Check3"))
	objMap.Put("Check4", Cursor1.GetDouble("Check4"))
	objMap.Put("Unit", Cursor1.GetDouble("Unit"))
	objMap.Put("Carton", Cursor1.GetDouble("Carton"))
	objMap.Put("Price", Cursor1.GetDouble("Price"))
	objMap.Put("Promotion", Cursor1.GetString("Promotion"))
	objMap.Put("StartDate", Cursor1.GetString("Start"))
	objMap.Put("EndDate", Cursor1.GetDouble("End"))
	objMap.Put("PromotionType", Cursor1.GetInt("Type"))
	objMap.Put("ErrorMsg", Cursor1.GetString("case_error"))
	objMap.Put("Comment", Cursor1.GetString("Comment"))
	objMap.Put("DMS", Cursor1.GetDouble("DMS"))
	objMap.Put("ShipDate", Cursor1.GetDouble("Shipdate"))
	objMap.Put("DateSync", Today)
    InsertData.Add(objMap) 
   Next 
    Cursor1.Close
	If CountforJson > 0 Then
		PostData.Put("insert", InsertData)
		Dim objJSon As JSONGenerator: objJSon.Initialize(PostData) 
	    Post_Daily.PostString(Main.Intsert_DailyReport, objJSon.ToPrettyString(2))
		'Msgbox(objJSon.ToPrettyString(4),"Gen JSON") 
	    'File.WriteString(File.DirRootExternal & "/Hi-Q", "PostDailyReport.txt",  objJSon.ToPrettyString(4))
	End If
	Catch
		Log(LastException.Message)
	End Try 
End Sub

Sub ImageView5_Click
 	'AutoSync
	Check_Out
	'Clear_Flag_Check 
	'Panel2.Visible = True
	PostUserTimeStamp
    PostCompetitor
    PostDailyReport 
	ToastMessageShow("Last....",False)
End Sub

Sub Check_Completed
Dim A1,A2 As Int
    A1 =0
	A2 =0
	CheckCoutCompleted = 0
    'ToastMessageShow(Tripplan.StoreID.SubString(0),False)
    SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
 Try
	Cursor2 = SQL1.ExecQuery("select Count(*) as count from CompetitorInfo  where status_check ='0' and StoreID  = '" & Tripplan.StoreID.SubString(0) & "'")
	Cursor2.Position = 0	    
	A1 =  Cursor2.GetString("count")
    Cursor2.Close
	Cursor2 = SQL1.ExecQuery("select Count(*) as count from StoreInfo  where status_check ='0' and BranchID  = '" & Tripplan.StoreID2.SubString(0) & "'")
	Cursor2.Position = 0	    
	A2 =  Cursor2.GetString("count")
	Cursor2.Close
	CheckCoutCompleted = A1+A2
'	ToastMessageShow(CheckCoutCompleted &" A1="&A1&" A2="& A2,False)
	If CheckCoutCompleted = 0 Then
	'  ImageView5.Visible = True
	Else
	'  ImageView5.Visible = False
	End If
 Catch
		Log(LastException.Message)
	End Try 	
End Sub