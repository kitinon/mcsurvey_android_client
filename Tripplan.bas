﻿Type=Activity
Version=3.82
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False 
	#IncludeTitle: False
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
    Dim StoreID,StoreID2,Check_In,DateIn As String
	
End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.
    Dim SQL1 As SQL 
	Dim Cursor2 As Cursor	 
	Private Label1 As Label
	Private ListView1 As ListView
	Dim Today As String
	Private ImageView2 As ImageView
	Private ListView2 As ListView
	Private Label2 As Label
	Private Button1 As Button
	Private Label3 As Label
End Sub

Sub Activity_Create(FirstTime As Boolean)
	'Do not forget to load the layout file created with the visual designer. For example:
	Activity.LoadLayout("Tripplan") 
	ListView1.Clear 
	
	StoreID = ""
	StoreID2 = ""
	Check_In = ""
	DateIn = ""	
	Today = DateTime.GetDayOfMonth(DateTime.Now)&"/"&DateTime.GetMonth(DateTime.Now)&"/"&DateTime.GetYear(DateTime.Now)
	ListView1.SingleLineLayout.Label.TextSize = 14dip
    ListView1.FastScrollEnabled = True
    ListView1.SingleLineLayout.ItemHeight = 40dip
    ListView1.SingleLineLayout.Label.TextColor = Colors.DarkGray
	Label3.Text = Today 
	'ToastMessageShow(DateTime.GetDayOfMonth(DateTime.Now)&"/"&DateTime.GetMonth(DateTime.Now)&"/"&DateTime.GetYear(DateTime.Now),False)
	Get_Trip
	CheckDaily.StatusDaily = 0
	CheckMatch.Competitor = 0
End Sub

Sub Activity_Resume
    Get_Trip
	CheckDaily.StatusDaily = 0 
	CheckMatch.Competitor = 0
End Sub

	
Sub CheckIn
 Dim Col1,Col2,Col7,Col3,Col8,Col4,Col6,Col5,Col9 As String
 ToastMessageShow("Check In...",False)
 'ToastMessageShow(DateUtils.TicksToString(DateTime.Now).SubString2(1,8) ,False)
 Col1 = Main.GPS_info
 Col2 = Main.UserID 
 Col3 = 0
 Col4 = Main.GPS_Latitude
 Col5 = Main.GPS_Longitude
 Col6 =  DateUtils.TicksToString(DateTime.Now)
 DateIn = DateUtils.TicksToString(DateTime.Now)
 Col7 =  StoreID2.SubString(0)
 Col8 = DateUtils.TicksToString(DateTime.Now)
 
 Check_In = DateUtils.TicksToString(DateTime.Now)
  
 SQL1.ExecNonQuery2("INSERT INTO CheckIn VALUES(?,?, ?, ?, ?, ?,? ,?,?)", Array As Object (Col1,Col2,Col3,Col4,Col5,Col6,Col7,Col8,Col9)) 
End Sub

Sub Get_Trip
Dim CountTrip =0 As Int
Dim MyDatabase As String
    ListView1.Clear
	MyDatabase = File.DirRootExternal &"/Hi-Q"
	'Msgbox(MyDatabase,MyDatabase) 
	
	SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	Try     
  	 Cursor2 = SQL1.ExecQuery("select * from TripPlan where UserID = '" &  Main.UserID & "' AND Date_Trip  = '" &  Today  & "' and status_sync=0")
	 CountTrip = Cursor2.RowCount
	 For P = 0 To Cursor2.RowCount - 1
	 Cursor2.Position = P
	 ListView1.AddTwoLinesAndBitmap(Cursor2.GetString("StoreID2"),   Cursor2.GetString("Store") & " "  & Cursor2.GetString("Branch"),LoadBitmap(File.DirAssets, "locationsurvey.png"))
	 'ListView1.AddTwoLines( Cursor2.GetString("StoreID2"), Cursor2.GetString("Date_Trip") & " "  & Cursor2.GetString("Store") & " "  & Cursor2.GetString("Branch"))
    ' ToastMessageShow(Main.UserID & "  " &  Today    ,False)
	  Next
	  Cursor2.Close
	Catch
		Log(LastException.Message)
	End Try 
	If CountTrip = 0 Then
	  Activity.Finish
	End If
End Sub	

Sub Get_StoreID2  
Dim MyDatabase As String
    MyDatabase = File.DirRootExternal &"/Hi-Q"
    SQL1.Initialize(File.DirRootExternal & "/Hi-Q", "MCSurveys.db", True)
	Cursor2 = SQL1.ExecQuery("select StoreID from TripPlan where StoreID2  = '" & StoreID2.SubString(0) & "'")

	 If Cursor2.RowCount <> 0 Then
	  Cursor2.Position = 0
	  StoreID = Cursor2.GetString("StoreID")
	  End If
    Cursor2.Close
End Sub

Sub ListView1_ItemLongClick (Position As Int, Value As Object)
'	 
'	StoreID2 = Value
'	Get_StoreID2
'	CheckIn
'	StartActivity(Menu)
End Sub

Sub ListView1_ItemClick (Position As Int, Value As Object)
Dim result As Int
	StoreID2 = Value 
result = Msgbox2("กรุณายืนยัน","ต้องการ Check In", "ตกลง", "", "ยกเลิก", LoadBitmap(File.DirAssets, "locationsurvey.png"))
If result = DialogResponse.Positive Then
	Get_StoreID2 
	CheckIn
	StartActivity(Menu)
End If

	
End Sub

